import React, { useState, useEffect } from 'react'
import { setValuesFromOptions } from './helpers'
import shortid from 'shortid'

export const DropDown = ({ children, options, onSelectItem, activeItem }) => {
  const [dropDownId] = useState(shortid.generate())
  const [active, setActive] = useState(() => activeItem)
  const [items] = useState(() => setValuesFromOptions(options))
  const [keyPressed, setKeyPressed] = useState()
  const [preSelect, setPreSelect] = useState(false)
  const [isVisible, setVisible] = useState(false)
  const [dropDownInFocus, setDropDownInFoucs] = useState(false)

  const itemKeys = Object.keys(items)
  const firstItem = items[itemKeys[0]]
  const currentActiveItem = active ? active : activeItem

  // preSelect is false unless onMouseOver item has set value. If so, make first item preSelected
  const currentPreSelect = preSelect ? preSelect : firstItem.label
  const indexOfPreSelect = itemKeys.indexOf(currentPreSelect)
  const nextSelect = itemKeys[indexOfPreSelect + 1]
  const previousSelect = itemKeys[indexOfPreSelect - 1]

  const handlePreSelect = ({ key }) => {
    if (dropDownInFocus === dropDownId) {
      if (key === 'ArrowDown' || key === 'ArrowRight') {
        // make sure not last item in list
        if (indexOfPreSelect !== itemKeys.length - 1) {
          preSelect ? setPreSelect(nextSelect) : setPreSelect(currentPreSelect)
        }
      }
      if (key === 'ArrowUp' || key === 'ArrowLeft') {
        // make sure not first item in list
        if (indexOfPreSelect !== 0) {
          setPreSelect(previousSelect)
        }
      }
      if (key === 'Enter') {
        setActive(preSelect)
        onSelectItem && onSelectItem(preSelect)
      }
    }
  }

  useEffect(() => {
    document.body.addEventListener('keydown', handlePreSelect)
    return () => {
      document.body.removeEventListener('keydown', handlePreSelect)
    }
  }, [preSelect, dropDownInFocus])

  return (
    <span
      style={{ display: 'inline-flex' }}
      onMouseEnter={() => {
        setDropDownInFoucs(dropDown => (dropDown = dropDownId))
        setPreSelect(false)
      }}
      onMouseLeave={() => {
        setDropDownInFoucs(false)
        setPreSelect(false)
      }}
    >
      {children({
        ...generateItems({
          itemKeys,
          currentActiveItem,
          setActive,
          onSelectItem,
          keyPressed,
          setKeyPressed,
          preSelect,
          setPreSelect,
          isVisible,
        }),
        show: () => setVisible(visible => !visible),
      })}
    </span>
  )
}

function generateItems({ itemKeys, currentActiveItem, setActive, onSelectItem, preSelect, setPreSelect }) {
  const makeSelected = value => _ => {
    onSelectItem && onSelectItem(value)
    setActive(value)
  }
  const makePreSelect = value => _ => setPreSelect(value)
  return {
    values: itemKeys.map(value => ({
      value,
      active: value === currentActiveItem,
      select: makeSelected(value),
      preSelect: preSelect === value,
      actions: {
        onMouseEnter: makePreSelect(value),
        onMouseOut: () => setPreSelect(false),
      },
    })),
  }
}
