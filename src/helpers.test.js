import { setValuesFromOptions } from './helpers'

describe('setValuesFromOptions', () => {
  test('when options is array, return objects with label and value set from array value', () => {
    const options = ['one', 'two', 'three']
    const setValues = setValuesFromOptions(options)
    const expectedValues = {
      one: { value: 'one', label: 'one' },
      three: { value: 'three', label: 'three' },
      two: { value: 'two', label: 'two' },
    }
    expect(setValues).toEqual(expectedValues)
  })
  test('when options is object with label and value properties, return new object with label and value set their respective properties', () => {
    const options = [
      { value: 'one', label: 'lableOne' },
      { value: 'two', label: 'lableTwo' },
      { value: 'three', label: 'lableThree' },
    ]
    const setValues = setValuesFromOptions(options)
    const expectedValues = {
      one: { value: 'one', label: 'lableOne' },
      three: { value: 'three', label: 'lableThree' },
      two: { value: 'two', label: 'lableTwo' },
    }
    expect(setValues).toEqual(expectedValues)
  })
  test('when options is object with only a value property, return new object with label and value set the value property', () => {
    const options = [{ value: 'one' }, { value: 'two' }, { value: 'three' }]
    const setValues = setValuesFromOptions(options)
    const expectedValues = {
      one: { value: 'one', label: 'one' },
      three: { value: 'three', label: 'three' },
      two: { value: 'two', label: 'two' },
    }
    expect(setValues).toEqual(expectedValues)
  })
  test('when options is object with not value property, return error', () => {
    const options = [{ label: 'lableOne' }, { label: 'lableTwo' }, { label: 'lableThree' }]
    expect(() => setValuesFromOptions(options)).toThrowError(
      'Is passing in a object of values, you must provide a value key',
    )
  })
})
