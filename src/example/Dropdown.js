import React, { useState } from 'react'
import { DropDown } from '../Dropdown'

const Drop = ({ onSelectItem, options }) => {
  return (
    <DropDown onSelectItem={x => onSelectItem(x)} options={options}>
      {props => {
        return (
          <div style={{ width: '300px', border: '2px solid black', display: 'flex', flexDirection: 'column' }}>
            <div onClick={props.show}>Drop Down</div>
            {props.values.map(value => {
              return (
                <div
                  {...value.actions}
                  onClick={value.select}
                  style={{
                    color: value.active ? 'steelblue' : 'white',
                    background: value.preSelect ? 'black' : 'lightgrey',
                  }}
                >
                  <div>{value.value}</div>
                </div>
              )
            })}
          </div>
        )
      }}
    </DropDown>
  )
}

const App = () => {
  const [selected, setSelected] = useState('No number selected')
  const [selected1, setSelected1] = useState('No name selcted')
  const [selected2, setSelected2] = useState('No food selected')

  return (
    <div>
      <h1>Number Selection: {selected}</h1>
      <Drop onSelectItem={value => setSelected(value)} options={['One', 'Two', 'Three', 'Four']} />
      <h1>Name selection: {selected1}</h1>
      <Drop onSelectItem={value => setSelected1(value)} options={['Bob', 'Jack', 'Jim', 'Jack']} />
      <h1>Food selection: {selected2}</h1>
      <Drop onSelectItem={value => setSelected2(value)} options={['Cheese', 'Ham', 'Beef', 'Turkey', 'Chicken']} />
    </div>
  )
}

export default App
