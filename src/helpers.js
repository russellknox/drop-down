const isArray = type => Array.isArray(type)
const isObject = type => typeof type === 'object'

export const setValuesFromOptions = options => {
  if (isArray(options)) {
    return options.reduce((state, option) => {
      if (isObject(option)) {
        if (!option.value) {
          throw new Error('Is passing in a object of values, you must provide a value key')
        }
        if (option.value && option.label) {
          return {
            ...state,
            [option.value]: {
              value: option.value,
              label: option.label,
            },
          }
        }
        return {
          ...state,
          [option.value]: {
            value: option.value,
            label: option.value,
          },
        }
      }
      return {
        ...state,
        [option]: {
          value: option,
          label: option,
        },
      }
    }, {})
  }
}
