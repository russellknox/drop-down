import React from 'react'
import { DropDown } from './Dropdown'
import { render, fireEvent, cleanup, waitForElement, flushEffects } from 'react-testing-library'

const Help = ({ error }) => {
  // console.log(error)
  return null
}

const setUpTests = props => (
  <DropDown {...props}>
    {({ values }) => (
      <>
        {values.map(value => (
          <div
            {...value.actions}
            key={value.value}
            onClick={value.select}
            className={[value.active ? 'active' : 'not-active', value.preSelect ? 'selected' : 'not-selected'].join(
              ' ',
            )}
            data-testid={`values-${value.value}`}
          >
            {value.value}
            <Help error={values} />
          </div>
        ))}
      </>
    )}
  </DropDown>
)
afterEach(cleanup)

it('renders child function', async () => {
  const Test = () => <div>sdf</div>
  const RenderProps = jest.fn(() => <Test />)
  render(<DropDown options={[1, 2]}>{RenderProps}</DropDown>)
  expect(RenderProps).toHaveBeenCalledTimes(1)
})

it('should render drop down values based on values props', () => {
  const options = [{ value: 'one' }, { value: 'two' }]
  const { queryAllByTestId } = render(setUpTests({ options }))
  expect(queryAllByTestId(/values/).length).toBe(2)
})

it('should set avtive item based on activeItem prop', () => {
  const options = [{ value: 'one' }, { value: 'two' }]
  const activeItem = 'two'
  const { queryByTestId } = render(setUpTests({ options, activeItem }))
  expect(queryByTestId('values-one').classList.contains('not-active')).toBe(true)
  expect(queryByTestId('values-two').classList.contains('active')).toBe(true)
})

it('should change active item to drop down item clicked on', () => {
  const options = [{ value: 'one' }, { value: 'two' }, { value: 'three' }]
  const { queryByTestId } = render(setUpTests({ options }))
  const itemThree = queryByTestId('values-two')
  expect(itemThree.classList.contains('not-active')).toBe(true)
  fireEvent.click(itemThree)
  expect(itemThree.classList.contains('active')).toBe(true)
})

it('should call onSelectItem when item is selected and passing in current item value', () => {
  const onSelectItem = jest.fn()
  const options = [{ value: 'one' }, { value: 'two' }, { value: 'three' }]
  const { queryByTestId } = render(setUpTests({ options, onSelectItem }))
  const itemThree = queryByTestId('values-three')
  fireEvent.click(itemThree)
  expect(onSelectItem).toHaveBeenCalledWith('three')
})

it('should change preselect item to first item when key down is pressed for first time and there is no current preselect', async () => {
  const options = [{ value: 'one' }, { value: 'two' }, { value: 'three' }]
  const { queryByTestId, container } = render(setUpTests({ options }))
  const itemOne = queryByTestId('values-one')
  const itemTwo = queryByTestId('values-two')
  await waitForElement(() => fireEvent.mouseEnter(container.firstChild))
  flushEffects()
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowDown' }))
  expect(itemOne.classList.contains('selected')).toBe(true)
  expect(itemTwo.classList.contains('not-selected')).toBe(true)
})

it('should change preselect item to second item when key down is pressed twice', async () => {
  const options = [{ value: 'one' }, { value: 'two' }, { value: 'three' }]
  const { queryByTestId, container } = render(setUpTests({ options }))
  const itemOne = queryByTestId('values-one')
  const itemTwo = queryByTestId('values-two')
  await waitForElement(() => fireEvent.mouseEnter(container.firstChild))
  flushEffects()
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowDown' }))
  flushEffects()
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowDown' }))
  expect(itemOne.classList.contains('not-selected')).toBe(true)
  expect(itemTwo.classList.contains('selected')).toBe(true)
})

it('should change preselect item to next value when right arrow pressed', async () => {
  const options = [{ value: 'one' }, { value: 'two' }, { value: 'three' }]
  const { queryByTestId, container } = render(setUpTests({ options }))
  const itemOne = queryByTestId('values-one')
  await waitForElement(() => fireEvent.mouseEnter(container.firstChild))
  flushEffects()
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowRight' }))
  expect(itemOne.classList.contains('selected')).toBe(true)
})

it('should change pre selected to previous value from index of current pre selected value with ArrowLeft pressed', async () => {
  const options = [{ value: 'one' }, { value: 'two' }, { value: 'three' }]
  const { queryByTestId } = render(setUpTests({ options }))
  const itemOne = queryByTestId('values-one')
  const itemTwo = queryByTestId('values-two')
  await waitForElement(() => fireEvent.mouseEnter(itemTwo))
  flushEffects()
  expect(itemOne.classList.contains('not-selected')).toBe(true)
  expect(itemTwo.classList.contains('selected')).toBe(true)
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowLeft' }))
  expect(itemOne.classList.contains('selected')).toBe(true)
  expect(itemTwo.classList.contains('not-selected')).toBe(true)
})

it('should change preselect item to when mouse over item', async () => {
  const options = [{ value: 'one' }, { value: 'two' }, { value: 'three' }]
  const { queryByTestId } = render(setUpTests({ options }))
  const itemOne = queryByTestId('values-one')
  const itemTwo = queryByTestId('values-two')
  await waitForElement(() => fireEvent.mouseEnter(itemOne))
  expect(itemOne.classList.contains('selected')).toBe(true)
  expect(itemTwo.classList.contains('not-selected')).toBe(true)
})

it('should maintain preselected item when item is first in list and ArrowUp is pressed', async () => {
  const options = [{ value: 'one' }, { value: 'two' }]
  const { queryByTestId } = render(setUpTests({ options }))
  const itemOne = queryByTestId('values-one')
  await waitForElement(() => fireEvent.mouseEnter(itemOne))
  flushEffects()
  expect(itemOne.classList.contains('selected')).toBe(true)
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowUp' }))
  expect(itemOne.classList.contains('selected')).toBe(true)
})

it('should maintain preselected item when item is last in list and ArrowDown is pressed', async () => {
  const options = [{ value: 'one' }, { value: 'two' }, { value: 'three' }]
  const { queryByTestId } = render(setUpTests({ options }))
  const itemThree = queryByTestId('values-three')
  await waitForElement(() => fireEvent.mouseEnter(itemThree))
  flushEffects()
  expect(itemThree.classList.contains('selected')).toBe(true)
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowDown' }))
  expect(itemThree.classList.contains('selected')).toBe(true)
})

it('should change pre selected to next value from index of current pre selected value when ArrowUp pressed', async () => {
  const options = [{ value: 'one' }, { value: 'two' }, { value: 'three' }]
  const { queryByTestId } = render(setUpTests({ options }))
  const itemOne = queryByTestId('values-one')
  const itemTwo = queryByTestId('values-two')
  await waitForElement(() => fireEvent.mouseEnter(itemTwo))
  flushEffects()
  expect(itemOne.classList.contains('not-selected')).toBe(true)
  expect(itemTwo.classList.contains('selected')).toBe(true)
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowUp' }))
  expect(itemOne.classList.contains('selected')).toBe(true)
  expect(itemTwo.classList.contains('not-selected')).toBe(true)
})

it('should change pre selected item to active item when hit enter on pre selceted item', async () => {
  const options = [{ value: 'one' }, { value: 'two' }, { value: 'three' }]
  const { queryByTestId } = render(setUpTests({ options }))
  const itemTwo = queryByTestId('values-two')
  expect(itemTwo.classList.contains('not-active')).toBe(true)
  await waitForElement(() => fireEvent.mouseEnter(itemTwo))
  flushEffects()
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'Enter' }))
  expect(itemTwo.classList.contains('active')).toBe(true)
})
