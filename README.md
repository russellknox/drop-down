### React Drop Down

Pass an array of items that will be rendered into a drop down menu of list items. DropDown accepts the following props:

### Props
- `options`: Array, is required. This can either be `[one, two, three]` where the value and key will be the same. Or you can pass `[{one: 1}, {two: 2}]` where you define both the key and value
- `activeItem`: default to false. You can define an initial item that will become selected on render
- `onSelectItem`: called everytime an item is selected
 
### Parameters have access to
- `values` is an array of items to map over. Each value gives you access to:
    - `active`: boolean whether the current item is the active item
    - `preSelect`: boolean to indicate the item the mouse is hovering over or arrow key transition lands on
    - `select`: func to make the current value seleccted,
    - `actions`: object with onMouseEnter and onMouseOut functions. Spead on your component if you want to preSelect by hovering over an item
 

 

 

 

 

 

 